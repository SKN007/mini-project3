import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class Week3Stack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    //Create an S3 Bucket and add bucket properties like versioning and encryption
    const s3Bucket = new cdk.aws_s3.Bucket(this, "mini-project3-skn", {
      bucketName: "mini-project3-skn",
      versioned: true,
      encryption: cdk.aws_s3.BucketEncryption.KMS_MANAGED
    })

  }
}
