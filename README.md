[![pipeline status](https://gitlab.com/SKN007/mini-project3/badges/main/pipeline.svg)](https://gitlab.com/SKN007/mini-project3/-/commits/main)

# Mini Project 3

## Requiements

Create S3 bucket using AWS CDK

Use CodeWhisperer to generate CDK code

Add bucket properties like versioning and encryption

## Steps

### Environment setup

1. Download Node.js
2. Install CDK
   ```
   npm install -g aws-cdk
   ```
3. Install AWS Toolkit in VS code

## Create and deploy CDK project

### Initialize a CDK project

```
cdk init app --language typescript
```

### Generate code by using Code Whisperer

![img](1.png)

After getting the code, customize it to my needs.

### Deploy

1. Complie project
   ```
   npm run build
   ```
2. Produce a CloudFormation template
   ```
   cdk synth
   ```
3. Deploy the generated CloudFormation template
   ```
   cdk deploy
   ```

## Screenshot Display

![img](3.png)

Bucket versioning is enabled and encryption is set correctly.

![img](4.png)

![img](6.png)

![img](5.png)
